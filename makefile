CPP = g++

CPPFLAGS=-std=c++11 

CCPINCLUDES=-Iinclude/

CCPLDFLAGS=-lsfml-graphics -lsfml-window -lsfml-system -lGoIO


VPATH=./src ./include ./obj
vpath %.cpp ./src


AmpViewer : AmpViewer.o Button.o IMark.o Sensor.o TimeMark.o Visor.o
	$(CPP) $(CPPFLAGS)  $^ -o $@ $(CCPLDFLAGS)


%.o : %.cpp
	$(CPP) $(CPPFLAGS) $(CCPINCLUDES) -c $^ -o $@
	

.PHONY: clean
clean:
	rm -r AmpViewer *.o