# AmpViewer

AmpViewer permite visualizar en una gráfica las medidas de intensidad de corriente eléctrica con respecto al tiempo, 
capturadas por el sensor de corriente eléctrica compatible con la interfaz Go! Link de Vernier Software & Technology, para Linux.

El programa AmpViewer está escrito en C++ y utiliza las librerías de GoIO SDK y SFML.

**Contenido**

- Requisitos previos a la compilación de AmpViewer
	- Instalación de requisitos
-  Compilación y ejecución de AmpViewer
	- Compilación
	- Ejecución
- Uso de AmpViewer


***



![Screenshot](https://gitlab.com/wormius/AmpViewer/raw/master/screenshot.png  "Screenshot")

***


## Requisitos previos a la compilación de AmpViewer

Para compilar y obtener el ejecutable, se necesita tener instalados:

- make
- g++
- SFML (libsfml-dev)
- GoIO SDK (libGoIO.so)

### Instalación de requisitos

Para instalar **make**, **g++** y **SFML**:

En Ubuntu y derivados:
~~~
sudo apt-get install make build-essential libsfml-dev
~~~

Para instalar **GoIO SDK**:

- Instalar dependencias:
~~~
sudo apt-get install aptitude
sudo aptitude install build-essential
sudo aptitude install automake
sudo aptitude install dpkg-dev
sudo aptitude install libtool
sudo aptitude install libusb-1.0-0-dev
~~~

- Descargar el repositorio: [GoIO SDK](https://github.com/VernierSoftwareTechnology/GoIO_SDK) 
- Descomprimir el paquete descargado
- Entrar en la carpeta ***src*** de GoIO SDK
- Localizar y abrir con un editor de textos el archivo ***autogen.sh***
- Remplazar la linea:

~~~
for am in automake-1.7 automake-1.8 automake-1.9  automake; do
~~~
por
~~~
for am in automake-1.7 automake-1.8 automake-1.9 automake-1.XX automake; do
~~~
donde ***automake-1.XX*** debe corresponder a la versión de automake que tienes instalada.

- En terminal, dentro de la carpeta ***src*** de GoIO SDK:
~~~
./build.sh
~~~

## Compilación y ejecución de AmpViewer

### Compilación
Escribir en terminal, dentro de la capeta de AmpViewer:
~~~
make
~~~

### Ejecución
Despues de compilar, escribir en terminal, dentro de la capeta de AmpViewer:
~~~
./AmpViewer
~~~

## Uso de AmpViewer

Antes de ejecutar AmpViewer, debe estar conectado el sensor de intensidad de corriente eléctrica a la interfaz  Go! Link.

Una vez que se ejecuta el programa aparecerá una pantalla con los siguiente controles:

- **Iniciar / Pausar**: Al dar clic en este control, se inicia o se detiene el registro de medidas en la gráfica. Invocar la función de pausa provocará que la siguiente llamada a iniciar, empiece desde el tiempo cero.

![Iniciar](https://gitlab.com/wormius/AmpViewer/raw/master/textures/play.png  "Iniciar") ![Pausar](https://gitlab.com/wormius/AmpViewer/raw/master/textures/pause.png  "Pausar")

- **Limpiar pantalla**: Borra la curva dibujada sin detener el registro de las medidas.

![Limpiar](https://gitlab.com/wormius/AmpViewer/raw/master/textures/reset.png  "Limpiar")

- **Ampliar/Reducir escala de tiempo:**: Hace *zoom in* o *zoom out*, respectivamente, a la escala de tiempo (eje horizontal). Borra la curva previamente dibujada sin detener el registro de las medidas.

![Ampliar escala de tiempo](https://gitlab.com/wormius/AmpViewer/raw/master/textures/plus.png  "Ampliar escala de tiempo") ![Reducir escala de tiempo](https://gitlab.com/wormius/AmpViewer/raw/master/textures/minus.png  "Reducir escala de tiempo")

- **Establecer Cero (Tara)**: Este control establece el valor cero para el sensor. Se recomienda utlizarlo al principio de cada experimento.

![Tarar](https://gitlab.com/wormius/AmpViewer/raw/master/textures/zero.png  "Tarar")



