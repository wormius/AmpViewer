#include "Button.h"

Button::Button(sf::String path, sf::Vector2f position)
:sf::Sprite()

{
    img.loadFromFile(path);
    img.setSmooth(true);
    this->setTexture(img);

    this->setOrigin(img.getSize().x/2, img.getSize().y/2);
    this->setPosition(position);






}

Button::~Button()
{
    //dtor
}

bool Button::update(sf::Vector2i mouse){

    bool active=false;
    if (this->getGlobalBounds().contains(sf::Vector2f(mouse))){
        this->setColor(sf::Color(220,220,220));
        active = true;
    }
    else this->setColor(sf::Color(255,255,255));

    return active;


}
