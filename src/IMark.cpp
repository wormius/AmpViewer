#include "IMark.h"

IMark::IMark(float value, float time)
:sf::RectangleShape(sf::Vector2f(3,3)),
ivalue(value),
itime(time)
{
    this->setFillColor(sf::Color(237,59,99));
    this->setOrigin(this->getLocalBounds().width/2,this->getLocalBounds().height/2);
}

IMark::~IMark()
{
    //dtor
}

void IMark::run(sf::Time dt, float velocity){
    sf::Vector2f    position;

    position = this->getPosition() + sf::Vector2f(-velocity*dt.asSeconds(),0);
    this->setPosition(position);



}
