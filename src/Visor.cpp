#include "Visor.h"

Visor::Visor(int WIDTH, int HEIGHT)
:
width(WIDTH),
height(HEIGHT),
fps(60),
bg_color(255,255,255),

sensor(),

measuring(false),

window(sf::VideoMode(WIDTH, HEIGHT), "Corriente Electrica",sf::Style::Default),

reset_button("textures/reset.png", sf::Vector2f(100,60)),
play_button("textures/play.png", sf::Vector2f(WIDTH-100,60)),
plus_button("textures/plus.png", sf::Vector2f(100,130)),
minus_button("textures/minus.png", sf::Vector2f(100,180)),
zero_button("textures/zero.png", sf::Vector2f(100,227)),

point(sf::Vector2f(2,2)),

time_axis(sf::LinesStrip, 2),
i_axis(sf::LinesStrip, 2),
i_line(sf::LinesStrip, 0),


font_n(),
font_s(),

init_mark(5.0),

mark_position(WIDTH-100, 475),


time_velocity(150)
{


    time_axis[0].position = sf::Vector2f(50,475);
    time_axis[0].color = sf::Color(0,0,0);
    time_axis[1].position = sf::Vector2f(width - 50,475);
    time_axis[1].color = sf::Color(0,0,0);


    i_axis[0].position = sf::Vector2f(width - 100,250);
    i_axis[0].color = sf::Color(0,0,0);
    i_axis[1].position = sf::Vector2f(width - 100,700);
    i_axis[1].color = sf::Color(0,0,0);


    init_mark.setFillColor(sf::Color(237,59,99));
    init_mark.setOrigin(init_mark.getLocalBounds().width/2,init_mark.getLocalBounds().height/2);
    init_mark.setPosition(mark_position);


    font_n.loadFromFile("textures/LeagueMono-ExtraBold.ttf");
    font_s.loadFromFile("textures/LeagueMono-ExtraBold.ttf");

    i_label.setFont(font_s);
    i_label.setCharacterSize(70);
    i_label.setColor(sf::Color(170,71,120));
    i_label.setString(sf::String("I =          mA"));
    i_label.setPosition(350,70);

    i_value.setFont(font_n);
    i_value.setCharacterSize(50);
    i_value.setColor(sf::Color(100,100,100));
    i_value.setString(sf::String(ToString(sensor.get_measure())));
    i_value.setOrigin(i_value.getLocalBounds().width/2, 0);
    i_value.setPosition(690,90);


    single_point.setFont(font_n);
    single_point.setCharacterSize(20);
    single_point.setColor(sf::Color(0,0,0));


}


Visor::~Visor() {
    //dtor
}

void Visor::run() {

    clock.restart();
    dt = sf::Time::Zero;
    dt_fps = sf::Time::Zero;
    dt_ivalue = sf::Time::Zero;
    dt_measures = sf::Time::Zero;
    time = sf::Time::Zero;


    sf::Texture* texture = new sf::Texture();
    bool changed = false;

    while (window.isOpen()){

        sf::Event       event;

        while(window.pollEvent(event)){
            if (event.type == sf::Event::Closed) window.close();
            if (event.type == sf::Event::KeyPressed)
                if (event.key.code == sf::Keyboard::Escape) window.close();

        }

        if (reset_button.update(sf::Mouse::getPosition(window))){
            if(event.type==sf::Event::MouseButtonPressed){

                if (event.mouseButton.button== sf::Mouse::Left && !changed){


                    time_mark.clear();
                    i_mark.clear();
                    i_line.clear();

                    changed = true;
                }


            }
            else if (event.type==sf::Event::MouseButtonReleased) changed = false;


        }
        if (play_button.update(sf::Mouse::getPosition(window)) ){
            if(event.type==sf::Event::MouseButtonPressed){

                if (event.mouseButton.button== sf::Mouse::Left && !changed){

                    if (!measuring){
                        time_mark.clear();
                        i_mark.clear();
                        i_line.clear();
                        delete texture;
                        texture = new sf::Texture();
                        texture->loadFromFile("textures/pause.png");
                        play_button.setTexture(*texture);

                        measuring = true;
                        time_measures_init = time;

                        time_mark.push_back(TimeMark());
                        time_mark[time_mark.size()-1].setPosition(width-100,475);

                        i_mark.push_back(IMark(sensor.get_measure(), time.asSeconds() - time_measures_init.asSeconds()));
                        i_mark[i_mark.size()-1].setPosition(width-100,475);

                        i_line.append(sf::Vertex(i_mark[i_mark.size()-1].getPosition(),sf::Color(237,59,99)));


                        dt_measures = sf::Time::Zero;
                    }
                    else{
                        delete texture;
                        texture = new sf::Texture();
                        texture->loadFromFile("textures/play.png");
                        play_button.setTexture(*texture);
                        measuring = false;

                    }

                    changed = true;
                }


            }
            else if (event.type==sf::Event::MouseButtonReleased) changed = false;


        }
        if (plus_button.update(sf::Mouse::getPosition(window))){
            if(event.type==sf::Event::MouseButtonPressed){

                if (event.mouseButton.button== sf::Mouse::Left && !changed){


                    time_mark.clear();
                    i_mark.clear();
                    i_line.clear();

                    time_velocity+=50;


                    changed = true;
                }


            }
            else if (event.type==sf::Event::MouseButtonReleased) changed = false;


        }
        if (minus_button.update(sf::Mouse::getPosition(window))){
            if(event.type==sf::Event::MouseButtonPressed){

                if (event.mouseButton.button== sf::Mouse::Left && !changed && time_velocity >= 60){


                    time_mark.clear();
                    i_mark.clear();
                    i_line.clear();

                    time_velocity-=50;


                    changed = true;
                }


            }
            else if (event.type==sf::Event::MouseButtonReleased) changed = false;


        }
        if (zero_button.update(sf::Mouse::getPosition(window))){
            if(event.type==sf::Event::MouseButtonPressed){

                if (event.mouseButton.button== sf::Mouse::Left && !changed){

                    time_mark.clear();
                    i_mark.clear();
                    i_line.clear();


                    sensor.set_zero();


                    changed = true;
                }


            }
            else if (event.type==sf::Event::MouseButtonReleased) changed = false;


        }






        if (measuring){


            if (dt_measures.asMilliseconds() >= 1000){

                time_mark.push_back(TimeMark());
                time_mark[time_mark.size()-1].setPosition(width-100,475);


                dt_measures = sf::Time::Zero;


            }


            for (int i=0; i<time_mark.size(); i++){
                time_mark[i].run(dt, time_velocity);
                if (time_mark[i].getPosition().x < 50) time_mark.pop_front();


            }


            for (int i=0; i<i_mark.size(); i++){
                i_mark[i].run(dt, time_velocity);

                i_line[i].position = i_mark[i].getPosition();

                if (i_mark[i].getPosition().x < 50)
                {
                 i_mark.pop_front();
                 i_line.resize(i_mark.size());

                }

            }



        }

        if (dt_ivalue.asSeconds() >= 0.5){
            i_value.setString(sf::String(ToString(sensor.get_measure())));
            i_value.setOrigin(i_value.getLocalBounds().width/2, 0);

            dt_ivalue = sf::Time::Zero;

        }







        while (dt_fps.asSeconds() >= 1.0f/fps){

            float measure = sensor.get_measure();
            mark_position.y = 475 - (measure*220.0f/600.0f); //SENSOR MEASURE *******************************************
            if (measuring){
                i_mark.push_back(IMark(measure, time.asSeconds()-time_measures_init.asSeconds()));
                i_mark[i_mark.size()-1].setPosition(mark_position );

                i_line.append(sf::Vertex(mark_position,sf::Color(237,59,99)));


            }

            for (int i=0; i < i_mark.size(); i++){

                sf::Vector2f        mouse;
                mouse = sf::Vector2f(sf::Mouse::getPosition(window));
                if ( mouse.x > i_mark[i].getPosition().x - i_mark[i].getLocalBounds().width/2.0
                    && mouse.x < i_mark[i].getPosition().x + i_mark[i].getLocalBounds().width/2.0
                    && mouse.y < i_mark[i].getPosition().y + 250.0
                    && mouse.y > i_mark[i].getPosition().y - 250.0 ) {

                    std::string     sep=" mA - ";
                    std::string     v,t;
                    v = ToString(i_mark[i].ivalue);
                    t = ToString(i_mark[i].itime);
                    single_point.setString(v+sep+t+" sec");
                    single_point.setPosition(mouse + sf::Vector2f(0,20));


                }
                else if (  mouse.y > i_mark[i].getPosition().y + 250.0
                        || mouse.y < i_mark[i].getPosition().y - 250.0 ){

                        single_point.setString("");
                }
            }// display single measure




            init_mark.setPosition(mark_position);








            window.clear(bg_color);



            window.draw(time_axis);
            window.draw(i_axis);



            for (int i=0; i<time_mark.size(); i++){

                window.draw(time_mark[i]);

            }

            for (int i=0; i<i_mark.size(); i++){

                window.draw(i_mark[i]);

            }


            window.draw(init_mark);



            window.draw(reset_button);
            window.draw(play_button);
            window.draw(plus_button);
            window.draw(minus_button);
            window.draw(zero_button);

            window.draw(i_line);


            window.draw(i_label);
            window.draw(i_value);

            window.draw(single_point);

            window.display();
            dt_fps = sf::Time::Zero;
        }


        dt = clock.getElapsedTime();
        clock.restart();

        time += dt;

        dt_fps += dt;
        dt_ivalue += dt;
        dt_measures +=dt;





    }


}

std::string Visor::ToString(float value)
{
    std::ostringstream ss;
    ss << value;
    return ss.str();
}
