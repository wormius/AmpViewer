#include "TimeMark.h"

TimeMark::TimeMark()
:sf::RectangleShape(sf::Vector2f(2,10))
{
    this->setFillColor(sf::Color(0,0,0));
    this->setOrigin(this->getLocalBounds().width/2,this->getLocalBounds().height/2);
}

TimeMark::~TimeMark()
{
    //dtor
}

void TimeMark::run(sf::Time dt, float velocity){
    sf::Vector2f    position;

    position = this->getPosition() + sf::Vector2f(-velocity*dt.asSeconds(),0);
    this->setPosition(position);



}
