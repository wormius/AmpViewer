#ifndef VISOR_H
#define VISOR_H

#include <deque>
#include <SFML/Graphics.hpp>
#include "Sensor.h"
#include "Button.h"
#include "TimeMark.h"
#include "IMark.h"

#include <cmath>
#include <sstream>
#include <string>

class Visor {
  public:


    Visor(int, int);
    virtual ~Visor();

    void        run();



    int         width;
    int         height;
    int         fps;

    sf::RenderWindow        window;
    sf::Color               bg_color;

    sf::Clock               clock;
    sf::Time                dt;
    sf::Time                dt_fps;
    sf::Time                dt_ivalue;
    sf::Time                dt_measures;
    sf::Time                time;
    sf::Time                time_measures_init;


    sf::RectangleShape      point;
    sf::VertexArray         time_axis;
    sf::VertexArray         i_axis;
    sf::VertexArray         i_line;



    Sensor                  sensor;
    bool                    measuring;

    Button                  reset_button;
    Button                  play_button;
    Button                  plus_button;
    Button                  minus_button;
    Button                  zero_button;

    std::deque<TimeMark>             time_mark;
    std::deque<IMark>                i_mark;

    sf::CircleShape         init_mark;
    sf::Vector2f            mark_position;

    float                   time_velocity;


    sf::Font                font_n;
    sf::Font                font_s;
    sf::Text                i_label;
    sf::Text                i_value;


    sf::Text                single_point;


  private:


    std::string                  ToString(float);









};

#endif // VISOR_H
