#ifndef BUTTON_H
#define BUTTON_H

#include <SFML/Graphics.hpp>

class Button : public sf::Sprite
{
    public:
        Button(sf::String path, sf::Vector2f position);
        virtual ~Button();

        bool update(sf::Vector2i mouse);


    private:
        sf::Texture img;
};

#endif // BUTTON_H
