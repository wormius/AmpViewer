#ifndef IMARK_H
#define IMARK_H

#include <SFML/Graphics.hpp>

class IMark : public sf::RectangleShape
{
    public:
        IMark(float,float);
        virtual ~IMark();
        void run(sf::Time, float);

        float       ivalue;
        float       itime;
};

#endif // IMARK_H
