#ifndef TIMEMARK_H
#define TIMEMARK_H

#include <SFML/Graphics.hpp>

class TimeMark : public sf::RectangleShape
{
    public:
        TimeMark();
        virtual ~TimeMark();
        void run(sf::Time, float);


};

#endif // TIMEMARK_H
